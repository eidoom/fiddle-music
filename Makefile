SRCS=$(wildcard *.abc)
PDFS=$(SRCS:abc=pdf)

.PHONY: all clean

all: ${PDFS}

%.pdf: %.svg
	for f in $(basename $<)*.svg; do rsvg-convert "$$f" -f pdf -o "$${f%svg}pdf"; done
	-rm $(basename $<)*.svg
	@# assume < 10 tunes
	pdfunite $(basename $<)0*.pdf "$@"
	-rm $(basename $<)0*.pdf

%.svg: %.abc style.fmt
	@# outputs files %001.svg for each tune (indexed by abc metadata field X)
	@# in landscape mode
	abcm2ps "$<" -O "$@" -g -l -F "$(word 2, $^)"

clean:
	-rm *.pdf *.svg *.mid
