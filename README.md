# [fiddle-music](https://gitlab.com/eidoom/fiddle-music)

## Songs

* Biddy from Sligo
* Crossing the Minch
* Marnie Swanson of the Grey Coast
* Mike Oldfield’s Single
* Mrs MacLeod of Raasay
* Niel Gow’s Lament for his Second Wife
* Paddy’s Leather Breeches
* The Bombardier Beetle
* The Dark Island
* The Headlands
* The Soup Dragon

## TODO

* Braigh Loch Iall p9
* Chi Mi na Mòr-Bheanna p15
* Lament for the Death of the Reverend Archie Beaton p20
* The Athole Highlanders p33
* Father John MacMillan of Barra p46
* The Jig of Slurs p77
* Lexie McAskill p82
* The Devil in the Kitchen p91
* Miss Drummond of Perth p91

## Dependencies

```shell
sudo dnf install make abcm2ps librsvg2-tools poppler-utils
```

## Usage

Generate pdfs with
```shell
make
```

## Notes

* There must be a blank line at the end of the `abc` file
* Similar: [Lilypond](https://lilypond.org/index.html)

## Further details

See [this post](https://computing-blog.netlify.app/post/abc/).

## Reference

* [abc specification documentation](http://abcnotation.com/wiki/abc:standard:v2.2)
